package com.example.a2lesson3;

import android.os.Bundle;
import android.app.Activity;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

public class MainActivity extends FragmentActivity {
	public static final String text = "text";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
			   FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			   listfragment listFragment = new listfragment();
			   int ori = getResources().getConfiguration().orientation;
			   if (ori == Configuration.ORIENTATION_PORTRAIT)
			   		ft.add(R.id.container, listFragment, "List_Fragment");
			   else if (ori == Configuration.ORIENTATION_LANDSCAPE)
				   ft.add(R.id.displayList, listFragment, "List_Fragment");
			   ft.commit();
				
	}	
	
	public void displaydetails(String t)
	{
		detailfragment df = new detailfragment();
		Bundle b = new Bundle();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		b.putString("detail",t);
		df.setArguments(b);
		int ori = getResources().getConfiguration().orientation;
		if (ori == Configuration.ORIENTATION_LANDSCAPE)
			ft.replace(R.id.displayDetail, df, "Detail_Fragment");	
		else if (ori == Configuration.ORIENTATION_PORTRAIT)
			ft.replace(R.id.container, df, "Detail_Fragment");	
		ft.commit();
		
	}
	
	public void displaylist()
	{
		listfragment lf = new listfragment();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		int ori = getResources().getConfiguration().orientation;
		if (ori == Configuration.ORIENTATION_LANDSCAPE)
			ft.add(R.id.displayList, lf, "List_Fragment");	
		else if (ori == Configuration.ORIENTATION_PORTRAIT)
			ft.replace(R.id.container, lf, "List_Fragment");	
		ft.commit();
	}
}
