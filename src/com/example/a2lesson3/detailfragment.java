package com.example.a2lesson3;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class detailfragment extends Fragment{
	TextView detail;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    View view = inflater.inflate(R.layout.detailfragment, container, false);
	     detail = (TextView) view.findViewById(R.id.detail);
	    Bundle b = getArguments();	 	    
	    detail.setText(b.getString("detail"));
	    Button back;
	    int ori = getResources().getConfiguration().orientation;
		if (ori == Configuration.ORIENTATION_PORTRAIT)
		{
	    	back = (Button) view.findViewById(R.id.backbutton);		
		    back.setOnClickListener(new OnClickListener(){
	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MainActivity ma =(MainActivity) getActivity();
					ma.displaylist();
					
				}
		    	    	
		    	
		    });
		}		
		
		if (savedInstanceState != null)
		{
			String dt = savedInstanceState.getString("detail");
			detail.setText(dt);
		}
		
	    return view;
	  }
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    outState.putString("detail", detail.getText().toString());
	  }
	

}
