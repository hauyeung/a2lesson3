package com.example.a2lesson3;


import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class listfragment extends Fragment{
	List<String> products = new ArrayList<String>();
	String[][] fruits;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.listfragment, container, false);		
	    return v;	  
	  }
	
	 @Override 
	    public void onActivityCreated(Bundle savedInstanceState) { 
		 	super.onActivityCreated(savedInstanceState); 
		 	fruits = new String[5][5];
		 	fruits[0][0] = "banana";
		 	fruits[0][1] = "yellow fruit";
		 	fruits[1][0] = "apple";
		 	fruits[1][1] = "red fruit";
		 	fruits[2][0] = "orange";
		 	fruits[2][1] = "orange fruit";
		 	fruits[3][0] = "pear";
		 	fruits[3][1] = "yellow fruit";
		 	fruits[4][0] = "grapes";
		 	fruits[4][1] = "purple fruit";
		 	products.add(fruits[0][0]);
			products.add(fruits[1][0]);
			products.add(fruits[2][0]);
			products.add(fruits[3][0]);
			products.add(fruits[4][0]);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.productslist, products);
			ListView l = (ListView) getView().findViewById(R.id.productslist);
			l.setAdapter(adapter);
			l.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> adapter, View v,
						int pos, long mylng) {
					// TODO Auto-generated method stub
					String detail = fruits[pos][1];
					MainActivity ma = (MainActivity) getActivity();
					ma.displaydetails(detail);
				}			
				
			});
		 
	 }
		
	

}
